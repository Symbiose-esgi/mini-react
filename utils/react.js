import { isClass } from './check.js'

//créer le code html
function handleHtmlElement(element, props, children) {
    const anElement = document.createElement(element);

    //Test state
    if(props) Object.keys(props).forEach(prop => appendProp(anElement, prop, props));

    children.forEach(child => {
        let childrenData = child[0];

        if( typeof childrenData === 'object' ) {
            let anElementChild = childrenElement(childrenData['element'], childrenData['children'], null);
            anElement.appendChild(anElementChild);
        } else {
            anElement.appendChild(document.createTextNode(child));
        }
    });

    return anElement;
}

function childrenElement(element, children, currentElement){
    let currentEl = currentElement;
    let lastElement;

    if( currentEl == null ){
        currentEl = document.createElement(element);
    } else {
        lastElement = currentEl.appendChild(document.createElement(element));
    }

    if( typeof children === 'object' ) {
        childrenElement(children['element'], children['children'], currentEl);
    } else {
        if( lastElement ){
            lastElement.appendChild(document.createTextNode(children));
        } else {
            currentEl.appendChild(document.createTextNode(children));
        }
    }

    return currentEl;
}

function appendProp(element, prop, props) {
    if (/^on.*$/.test(prop)) {
      return element.addEventListener(prop.substring(2).toLowerCase(), props[prop]);
    } else {
      return element.setAttribute(prop, props[prop]);
    }
}

function handleClass(clazz) {
    const component = new clazz();
    return component.render();
}

function anElement(element, props, ...children) {
    if (isClass(element)) {
        return handleClass(element, props);
    } else {
        return handleHtmlElement(element, props, children);
    }
}

//créer un élément comportant le necessaire pour avoir du code html
export const createElement = (element, props, ...children) => {
    return anElement(element, props, children);
}