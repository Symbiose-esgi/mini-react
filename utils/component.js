export class Component {
    constructor(props) {
        this.props = props;
        this.state  = {};
    }
    setState(state) {
		this.state = { ...this.state, ...state};
		shouldUpdate(this.state);
	}
}

function shouldUpdate(state) {
	//test pour le state
	let idDocument = Object.keys(state)[0];
	let data = document.getElementById("state_"+idDocument);
	data.innerHTML = '';
	data.appendChild(document.createTextNode(state[idDocument]));
}