import { render } from './utils/dom.js';

import {Err404} from '../Component/404/index.js';
import {Test} from '../Component/Test/index.js';
import {Compteur} from '../Component/Compteur/index.js';
import {Home} from '../Component/Home/index.js';

const Router = function() {
    let routes = {};
    
    // Ajout de component
    this.addRoute = (path, component) => {
        routes[path] = component;
    }

    // Rendering
    this.main = () => {
        const root = document.getElementById('root');

        let path = Object.values(routes).find(function(el) {
            return routes[window.location.hash] === el;
        });

        if(path) {
            render(root, path);
        } else {
            render(root, Err404);
        }
    }
}

export const router = new Router();

router.addRoute("#/Test", Test);
router.addRoute("#/Compteur", Compteur);
router.addRoute("#/", Home);
router.addRoute("", Home);