import { createElement } from '../../utils/react.js';
import { Component } from '../../utils/component.js';

export class Test extends Component {
    constructor(props) {
        super(props)
    }
    
    render() {
    	let data = [];

    	let rowHeader = createElement('header',null,
            		{ "element" : "div", "props" : null, "children": 
            			{ "element" : "p", "props" : null, "children": "Ceci est un header" }
            		}
            	);

    	let rowNav = createElement('nav',null,
            		{ "element" : "div", "props" : null, "children": 
            			{ "element" : "p", "props" : null, "children": "Ceci est une nav" }
            		}
            	);

    	let rowSection = createElement('section',null,
            		{ "element" : "p", "props" : null, "children": 
            			{ "element" : "span", "props" : null, "children": "Ceci est une section" }
            		}
            	);

    	data.push(rowHeader);
    	data.push(rowNav);
    	data.push(rowSection);

        return data;
    }
}